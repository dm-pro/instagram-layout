const addClass = ( className, context ) => context.classList.add( className ),
  removeClass = ( className, context ) => context.classList.remove( className ),
  hasClass = ( className, context ) => context.classList.contains( className );
class iLayout {
  constructor( container ) {
    this.container = container;
    this.positionsContainer = container.querySelector( '.layout__positions' );
    this.actionButton = container.querySelector( '.layout__button' );
    this.result = container.querySelector( '.layout__result' );
    this.layout = {
      left: null,
      top: null,
      bottom: null
    };
    this.registerEvents();
  }
  registerEvents() {
    const layoutItems = this.positionsContainer.querySelectorAll('.layout__item'),
          canvas = document.createElement('canvas'),
          ctx = canvas.getContext('2d');
    canvas.width = this.positionsContainer.offsetWidth;
    canvas.height = this.positionsContainer.offsetHeight;

    for (let item of layoutItems) {
      item.addEventListener('dragover', this.addClassActive);

      item.addEventListener('dragleave', this.removeClassActive);

      item.addEventListener('drop', event => {
        this.removeClassActive(event);
        this.canvasDrawImage(event, item, ctx);
      });
    }

    this.actionButton.addEventListener('click', event => {
      event.preventDefault();
      this.collageGenerate(canvas);
    });
  }

  addClassActive( event ) {
    event.preventDefault();
    event.currentTarget.classList.add('layout__item_active');
  } 

  removeClassActive( event ) {
    event.preventDefault();
    event.currentTarget.classList.remove('layout__item_active');
  } 

  canvasDrawImage( event, item, ctx ) {
    const imageTypeRegExp = /^image\//;
    if (!(imageTypeRegExp.test(event.dataTransfer.files[0].type))) {
      this.result.value = 'Ошибка! Загруженный файл не является изображением';
      return;
    }

    return new Promise(resolve => {
      let img = document.createElement('img');
      img.src = URL.createObjectURL(event.dataTransfer.files[0]);
      img.classList.add('layout__image');
      img.addEventListener('load', event => {
        URL.revokeObjectURL(img.src);
        resolve([img, item, ctx]);
      });
      item.appendChild(img);
    })
      .then(arr => this.drawPic(arr));
  }

  drawPic( arr ) {
    let img = arr[0],
        item = arr[1],
        ctx = arr[2],
        x, y;
    if (item.classList.contains('layout__item_left')) {
      x = 0;
      y = 0;
    }
    if (item.classList.contains('layout__item_top')) {
      x = this.positionsContainer.offsetWidth / 2;
      y = 0;
    }
    if (item.classList.contains('layout__item_bottom')) {
      x = this.positionsContainer.offsetWidth / 2;
      y = this.positionsContainer.offsetHeight / 2;
    }
    ctx.drawImage(img, x, y, item.offsetWidth, img.offsetHeight);
  }

  collageGenerate( canvas ) {
    if (this.positionsContainer.querySelector('.layout__image') !== null) {
      let finalImage = document.createElement('img');
      finalImage.src = canvas.toDataURL();
      this.result.value = finalImage.outerHTML;
      this.positionsContainer.appendChild(finalImage);
    }
  }
}

new iLayout( document.getElementById( 'layout' ));